#include <stdio.h>
#include "functions.h"

int main()
{
	printf("5+4=%i\n",add(5,4));
	printf("5-4=%i\n",subtract(5,4));
	printf("5*4=%i\n",multiply(5,4));
	printf("5/4=%f\n",divide(5.0f,4.0f));
	printf("max(5,4)=%i\n",max(5,4));
	printf("min(5,4)=%i\n",min(5,4));
	return 0;
}
